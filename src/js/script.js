"use strict";

// Header menu button
const headMenuBtn = document.getElementById("headerMenuBtn");
const headMenuBtnLines = document.getElementById("headerMenuBtnLines");
const menu = document.getElementsByClassName("header-menu")[0];

headMenuBtn.addEventListener("click", function () {
  menu.classList.toggle("header-menu--active");
  headMenuBtnLines.classList.toggle("header-menu-btn__lines");
  headMenuBtnLines.classList.toggle("header-menu-btn__lines--active");
});
