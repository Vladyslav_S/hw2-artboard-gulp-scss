const { series, parallel } = require("gulp");
const { scripts } = require("./gulp-tasks/scripts");
const { styles } = require("./gulp-tasks/styles");
const { serv } = require("./gulp-tasks/serv");
const { watch } = require("./gulp-tasks/watch");
const { img } = require("./gulp-tasks/img");
const { del } = require("./gulp-tasks/del");

exports.build = series(del, series(styles, scripts, img));

exports.dev = parallel(serv, watch, series(styles, scripts));
